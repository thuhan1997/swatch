<?php
namespace App\Repositories\Statistic;
use App\Repositories\EloquentRepository;
use App\Statistic;
/**
 * 
 */
class StatisticEloquentRepository extends EloquentRepository implements StatisticRepositoryInterface
{
	protected $model;
	public function __construct(Statistic $model)
	{
		$this->model= $model;
	}
	
}
?>