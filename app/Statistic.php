<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $table='statistics';
    protected $fillable = [
        'ip','date',
    ];
    //protected $timestamp= false;
    
}
