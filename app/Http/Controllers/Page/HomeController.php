<?php

namespace App\Http\Controllers\Page;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\Sale\SaleRepositoryInterface;
use App\Repositories\Statistic\StatisticRepositoryInterface;
use Illuminate\Http\Request;
use App\Statistic;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    private $productRepository;
    private $saleRepository;
    private $statisticRepository;

    //* construct with middleware
     public function __construct(ProductRepositoryInterface $productRepository, SaleRepositoryInterface $saleRepository, StatisticRepositoryInterface $statisticRepository)
        {
    //     $this->middleware('auth');
        $this->productRepository=$productRepository;
        $this->saleRepository= $saleRepository;
        $this->statisticRepository= $statisticRepository;

        }
    public function index(){
        $newPr=$this->productRepository->getNewProduct();
        $specialPr=$this->productRepository->getSpecialProduct();
        $salePr= $this->saleRepository->getSale();
        return view('page.home',compact('newPr','specialPr','salePr'));
    }

    public function getSearch(Request $request){
        // print_r($request->key);
        $product =$this->productRepository->getSearch($request->key);
        $key= $request->key;
         return view('page.search', compact('product','key'));
         // print_r($product);
    }

    public function statistic(Request $request ){
        $ip= Statistic::where('ip',$request->ip)->where('date',$request->date)->get();
        if(count($ip)!=0) return response()->json(["existed"=>true]);
        else{
       
        $this->statisticRepository->create($request->all());
         return response()->json(["success"=>true]);
        }
    }
}
