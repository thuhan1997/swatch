<?php

namespace App\Http\Controllers\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Statistic;
use Auth;
class LoginController extends Controller
{
	public function __construct(){    

    }

    public function getLogin()
    {
    	//return view('Admin.login');
    	 
    
        if (Auth::check()) {
            // nếu đăng nhập thàng công thì 
            return redirect()->route('user.index');
        } else {
            return view('page.index');
        }
    }

}