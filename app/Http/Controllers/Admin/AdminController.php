<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;



class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');

        // $visitor= $_SERVER['REMOTE_ADDR'];

        // print_r($visitor);
    }
    public function track(Request $request){
        $data=array();
        for($i=1;$i<=12;$i++){
            $collection = Order::whereYear('date_order',$request->year)->whereMonth('date_order',$i);
           $data[]=$collection->sum('total_price');       
        }
        return response()->json($data);
        
    }
}
