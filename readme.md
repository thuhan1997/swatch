# JVB - Swatch: Laravel 5.7 integrated with Admin LTE
This project is **Laravel 5.7** integrated with **Admin LTE-2.3.11**

## How to Install
- Download this repo & extract or clone using Git
- Install project dependency by running `composer install` or `composer update `
- Copy `.env.example` to `.env`
- Edit .env file with `DB_DATABASE=swatch`
- Run `php artisan serve`
- Open `localhost:8000\admin`,  `localhost:8000`



